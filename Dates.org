#+title: Dates
#+HTML_HEAD: <link rel="stylesheet" type="text/css" href="https://gongzhitaao.org/orgcss/org.css"/>

* Dates

Obj : Master file for dates, contains dates from History, Polity and Economics. They have not been divided by Chapter but by their Year or Centuries
** Before Common Era
- 2600 BCE = Harappan Civilisation
- 1000 BCE = Early Iron
- 600 - 400 BCE = Early Historic
- 500 BCE = Panini Ashtadhyayi
- 500 - 400 BCE = Mahabharat and Ramayan
- 200 BCE to 200 CE = Manusmriti
- 327 BCE = Alexander Invades   
- 321 BCE = Mauryan Empire
** 100 - 200 CE (2nd Century)
- Kushana Rulers release 1st gold coins
- Satvahana and Shaka rulers
** 300 - 400 CE (4th Century)
- 320 CE = Begainning of Gupta Rule
** 600 - 700 CE (6th Century)
- Finds of gold coins reduces maybe due to economic recession or high circulation 
** 700 - 800 (8th Century)
- 711 Arab General Mohd Qasim conquered Sind
** 1200 - 1300 (13th Century)
- Formation of the Lingayats or Virashivas
- The Sufi Saints Year of Death
	- 1235 = Muinuddin Sizi
	- 1235 = Qutubuddin Bhaktiyar Kaki
	- 1265 = Fariduddin Ganj-i-Shikar
- Delhi Sultanate 
** 1300 - 1400 (14th Century)
- 1325 = Nizamuddin Auliya (Year of Death) 
- 1356 = Chiraj-i Delhi (Year of Death)
- 1336 = Harihara and Bukka create Vijaynagar Empire
** 1400 - 1500 (15th Century)
- 1469 = [Guru Nanak](Guru Nanak)
- Saints (no date)
  - Mira Bai
  - Kabir
  - Vallabhacharya
  - Shankaradeva
  - Tukaram
- 1485 = End of Sangama Dynasty (1st of VE); Begainning of Suluva Dynasty
- 1498 = Portuguese come to Vijaynagar Empire 

** 1500 - 1600 (16th Century)
- Saints
	- Chaitanya
	- Mirabai
	- Tulsidas
	- Malik Mohammad Giyasi (Padmavat)
- 1503 = Suluva ends; Tuluva begins
- 1542 = Tuluva ends; Aravidu begins
- 1565 = War of Rakshasi Tagadhi or Talikota; Leads to end of Vijaynagar; Against Sultans
- 1565 = Vijaynagar defeated 

** 1700 - 1800 (18th Century)
- 1754 = Colin Mackenzie was born
- 1784 = Asiatic Society of Begal was formed
- 1787 = US became independent 

** 1800 - 1900 (19th Century)
- 1800 = Colin Mackenzie discovers Hampi
- 1814 = Museum in Calcutta
- 1838 = James Princep deciphers Brahmi
- 1850 = Introduction of Railway
- 1869 = Suez Canal
- 1875 = Report of Alex Cunningham on Harappan Seal
- 1881 = 1st Census
- 1880 = HH Cole curator of monuments
** 1900 - 2000 (20th Century) 
- 1902 = Conservation by John Marshall
- 1907 = Tata Steel
- 1914 to 1918 = WW1
- 1917 = Russian Revolution
- 1919 = Critical Edition of Mahabharat starts
- 1921 = MS Vats excavates in Harappa; Year of Great Divide 
- 1922 = John Marshall makes existence of Harappa official
- 1923 = John Marashall writes the Conservation Manual
- 1925 = Mohenjodaro Excavations
- 1935 to 1945 = WW2; RBI Formation
- 1944 = Formation of the IMF (Bretton Woods Conference); World Bank also founded
- 1945 = Hiroshima and Nagasaki; June = Signing of the UN Charter; October 24 = UN was founded; October 30 = India joined the UN
- 1946 = REM Wheeler Excavates in Harappa
- 1947 = India gained Independence; Asian Relations Conference
- 30 January 1948 = Mahatma Gandhi dies
- Sept 1948 = Hyderabad becomes a part of ROI; General Agreement on Trade and Tariff
- 1949 = NATO is Formed; Conference organised by India to suppor Indonesian independence
- 1950 = Planning Commission was formed; World Health Organisation
- 1951 = India's 1st Five Year Plan was formulated
- 1952 = Vishalandhra State was formed
- 1953 = The State Reorganisation Committee was formed
- 1954 = Panchsheel Agreement; Vietnam divided along the 17th Parallel
- 1955 = Warsaw Pact; Karve Committee 
- 1956 = State Reorganisation Act was released; British attach Egypt on the Suez Canal issue; IPR 2
- 1957 = International Atomic Energy Agency
- 1960 = Maharastra and Gujrat was formed from Bombay
- 1961 = Belgrade Conference; Berlin Wall was built
- 1962 = Cuban Missle Crises; Indo China War; Modernisation of Indian Military
- 1963 = Limited Test Ban Treaty
- 1964 = Jawaharlal Nehru no longer Foreign Minister of India (1946 - 1965); Split in CPI into CPI and CPI-M; China does nuclear tests
- 1965 = Membership of the UNSC extended from 11 - 15
- 1966 = Punjab, Haryana and Himachal Pradesh formed; Critical Edition of the Mahabharatha is Completed; *White Revolution*
- 1967 = Green Revolution 
- 1969 = SALT-1 (Stretegic Arms Limitation Talks 1); Nationalisation of Banks; MRTP Monopolies and Restrictive Trade Policies Act
- 1970 = Nuclear Non Proliferation Treaty comes into force
- 1971 = India signs Friendship Agreement with USSR for 20 years; India invades East Pakistan and wins
- 1972 = Assam divided into Meghalaya, Manipur and Tripura; Shimla Agreement signed (Indira and Zulfikar Bhutto)
- 1973 = Buitenen starts translation of the Critical Edition; Foreign Exchange Regulation Act 
- 1974 = India conducts its first nuclear test
- 1976 = Hampi declared as World Heritage Site
- 1979 = USSR invades Afghanistan; Indo China relations resume after 1962 war; SALT - II
- 1980 = Italian team begins excavations
- 1982 = NABARD
- 1985 = Gorbachev becomes the Chairperson of the CCP
- 1986 = American Team excavates in Harappa
- 1987 = Arunachal and Mizoram formed
- 1989 = Berlin Wall broken
- 1990 = Unification of East and West Germany; Lithuania gains independence; RS Bisht begins excavations in Dholavira
- 1991 = Disintegration of USSR; New Economic Policy (Industries also)
- June 1991 = Boris Yeltsin becomes president of Russia
- December 1991 = Void the 1922 agreement of the formation of the USSR; Create 15 independent states
- 25 December 1991 = Gorbachev resigns as President of the USSR and the USSR is formally disintegrated
- 1993 = START (Stretegic Arms Reduction Treaty) - II; PM Rozgar Yojna
- 1995 = World Trade Organisation; National Social Assistence Program
- 1997 = Inquiry by Kofi Anan on the reforms to the UN; Swarnajayanti Shahri Rozgar Yojna
- 1998 = Tapas Majumdar Committee
- 1999 = Kargil Conflict; Foreign Exchange Management Act; Swarnajayanti Gram Rozgar Yojna
** 2000 - 2100 (21st Century)
- 2000 = Chattisgarh, Jharkhand and Uttarakhand formed; Gram Sadak Yojna
- 2001 = Tajikistan 10 year Civil War ends; VAAY
- 2001 = Indo-Russian Strategic Agreement
- 11 Sept 2001 = Terror attacks in the US 9/11
- 2005 = MGNREGA
- 2009 = RTE
- 2012 = Special Economic Zones
- 2013 = National Urban Livelyhood Mission
- 2014 = Telangana formed; Jan Dhan Yojna; Make In India
- 2015 = NITI Aayog
- 8th of November 2016 = Demonatisation
- 2017 = GST 

